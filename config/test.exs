use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :pxblog, Pxblog.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :pxblog, Pxblog.Repo,
  adapter: Ecto.Adapters.Postgres,
#  username: "postgres",
#  password: "postgres",
  username: System.get_env("DATABASE_POSTGRESQL_USERNAME") || "postgres",
  password: System.get_env("DATABASE_POSTGRESQL_PASSWORD") || "postgres",
  database: "pxblog_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# when in test env do minimal (4) encryption
config :comeonin, bcrypt_log_rounds: 4
