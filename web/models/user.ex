defmodule Pxblog.User do
  use Pxblog.Web, :model

  # pull in the Bcrypt module under the Comeonin namespace and import the hashpwsalt/1 function
  import Comeonin.Bcrypt, only: [hashpwsalt: 1]

  schema "users" do
    field :username, :string
    field :email, :string
    field :password_digest, :string


    belongs_to :role, Pxblog.Role
    has_many :posts, Pxblog.Post

    timestamps

    # virtual as these do not exist in the db, but need to exist as properties in the User struct.
    # also allows us to apply transformations in the changeset function.
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
  end

  @required_fields ~w(username email password password_confirmation role_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> hash_password
  end

  defp hash_password(changeset) do
    if password = get_change(changeset, :password) do
      changeset
      |> put_change(:password_digest, hashpwsalt(password))
    else
      changeset
    end
  end

end
