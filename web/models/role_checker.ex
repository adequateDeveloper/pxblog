defmodule Pxblog.RoleChecker do
  alias Pxblog.Repo
  alias Pxblog.Role

  # verify if the user is indeed an admin
  def is_admin?(user) do
    (role = Repo.get(Role, user.role_id)) && role.admin
  end
end