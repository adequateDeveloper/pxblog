defmodule Pxblog.UserView do
  use Pxblog.Web, :view

  # select boxes, for the values argument, requires either a list or a keyword list, either in the form of
  # [value, value, value] or [displayed:value, displayed:value].
  # we want to display the role name but have it carry the id value in our form submit. We can't just use @roles
  # in there because it doesn't adhere to either format.

  # takes a collection of roles.
  # pipe the collection into enum.map (&/&1 is shorthand for an anonymous function).
  # the equivilent of 'Enum.map(&["#{&1.name}": &1.id])' is 'Enum.map(roles, fn role -> ["#{role.name}": role.id] end)'
  # the Enum.map function returns a list of keywords where the name of the role is the key and the id of the role is
  # the value. Example:
  #     roles = [%Role{name: "Admin Role", id: 1}, %Role{name: "User Role", id: 2}] the map call would return:
  #     [["Admin Role": 1], ["User Role": 2]]
  # this is then piped into List.flatten which produces '["Admin Role": 1, "User Role": 2]' which is the format the
  # select form helper is expecting.
  def roles_for_select(roles) do
    roles
    |> Enum.map(&["#{&1.name}": &1.id])
    |> List.flatten
  end
end
