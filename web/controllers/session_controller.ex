defmodule Pxblog.SessionController do
  use Pxblog.Web, :controller

  import Comeonin.Bcrypt, only: [checkpw: 2]

  alias Pxblog.User

  # function that cleans up any user input, such as a blank string, which will be converted into a nil value
  plug :scrub_params, "user" when action in [:create]

  # login view
  def new(conn, _params) do
    render conn, "new.html", changeset: User.changeset(%User{})     # empty User struct
  end

  # login submission
#  def create(conn, %{"user" => user_params}) do
#    Repo.get_by(User, username: user_params["username"])    # pull the 1st User from the Repo that has a matching username or else return nil
#    |> sign_in(user_params["password"], conn)               # chain the user into the sign_in function
#  end

  def create(conn, %{"user" => %{"username" => username, "password" => password}})
    when not is_nil(username) and not is_nil(password) do
    user = Repo.get_by(User, username: username)
    sign_in(user, password, conn)
  end

  def create(conn, _params) do
    failed_login(conn)
  end

  # logout
  def delete(conn, _params) do
    conn
    |> delete_session(:current_user)
    |> put_flash(:info, "Signed out successfully!")
    |> redirect(to: page_path(conn, :index))
  end

  # when user is nil
  defp sign_in(user, password, conn) when is_nil(user) do
    failed_login(conn)
  end

  # when user is not nil
  defp sign_in(user, password, conn) do
    if checkpw(password, user.password_digest) do
      conn
      |> put_session(:current_user, %{id: user.id, username: user.username, role_id: user.role_id})
      |> put_flash(:info, "Sign in successful!")
      |> redirect(to: page_path(conn, :index))
    else
      failed_login(conn)
    end
  end

  defp failed_login(conn) do
    conn
    |> put_session(:current_user, nil)
    |> put_flash(:error, "Invalid username/password combination!")
    |> redirect(to: page_path(conn, :index))
    |> halt()
  end

end