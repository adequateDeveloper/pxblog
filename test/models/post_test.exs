defmodule Pxblog.PostTest do    # application namespace Pxblog
  use Pxblog.ModelCase          # use the functions & DSL in support/model_case.ex macro set

  alias Pxblog.Post             # insure the test has visibility into the Post model

  import Ecto.Changeset, only: [get_change: 2]  # needed to pull the translated value out of the changeset.

  # set up basic valid/invalid attributes for creating valid/invalid changesets
  # in this situation, will receive data from the controller in the form of string-based keys, not atoms
  @valid_attrs %{"body" => "some content", "title" => "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    # create a changeset from the Post model providing a blank Post struct and a map of valid parameters
    changeset = Post.changeset(%Post{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    # create a changeset from the Post model providing a blank Post struct and an empty parameters map
    changeset = Post.changeset(%Post{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "when the body includes a script tag" do
    changeset = Post.changeset(%Post{}, %{@valid_attrs | "body" => "Hello <script type='javascript'>alert('foo');</script>"})
    refute String.match? get_change(changeset, :body), ~r{<script>}
  end

  test "when the body includes an iframe tag" do
    changeset = Post.changeset(%Post{}, %{@valid_attrs | "body" => "Hello <iframe src='http://google.com'></iframe>"})
    refute String.match? get_change(changeset, :body), ~r{<iframe>}
  end

  test "body includes a link tag" do
    changeset = Post.changeset(%Post{}, %{@valid_attrs | "body" => "Hello <link>foo</link>"})
    refute String.match? get_change(changeset, :body), ~r{<link>}
  end

  test "body includes no stripped tags" do
    changeset = Post.changeset(%Post{}, @valid_attrs)
    assert get_change(changeset, :body) == @valid_attrs["body"]
  end

end
