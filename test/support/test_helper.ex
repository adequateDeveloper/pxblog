defmodule Pxblog.TestHelper do

  alias Pxblog.Repo
  alias Pxblog.User
  alias Pxblog.Role
  alias Pxblog.Post

  import Ecto, only: [build_assoc: 2]

  # insert of a role changeset
  # we expect a map that includes a role name and whether it’s admin or not.
  # We’re using Repo.insert here which means we’ll be returning the standard {:ok, model} response on successful insertions.
  def create_role(%{name: name, admin: admin}) do
    Role.changeset(%Role{}, %{name: name, admin: admin})
    |> Repo.insert
  end

  # Take a role, build an associated user, prep it for insertion into the database, and then insert it.
  # first argument is the role we want to use and the second argument a map of parameters to use to create our user.
  # pipe the role into the build function, creating a User model, which then gets piped into User.changeset.
  def create_user(role, %{email: email, username: username, password: password, password_confirmation: password_confirmation}) do
    role
    |> build_assoc(:users)
    |> User.changeset(%{email: email, username: username, password: password, password_confirmation: password_confirmation})
    |> Repo.insert
  end

  # Take a user, build an associated post, prep it for insertion into the database, and then insert it.
  def create_post(user, %{title: title, body: body}) do
    user
    |> build_assoc(:posts)
    |> Post.changeset(%{title: title, body: body})
    |> Repo.insert
  end
end