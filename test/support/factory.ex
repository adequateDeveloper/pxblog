defmodule Pxblog.Factory do

  # tell ExMachina that we’re using Ecto as our Repo layer and that it should act accordingly for create/associations/etc.
  use ExMachina.Ecto, repo: Pxblog.Repo

  alias Pxblog.Role
  alias Pxblog.User
  alias Pxblog.Post
  alias Pxblog.Comment

  # ExMachina:
  # build:  build means that we are building out the model (not a changeset) but not persisting it to the database.
  # create: create, however, DOES persist the model to the database.

  # factory/1:
  # return a Role struct that defines the default properties we want it to have.
  # the parameter pattern matchs on an atom for which type of factory to build/create from
  # the function is using a sequence to generate different names per each role we create

  # We want to generate a unique name for each role, so for us to be able to do so we define a sequentially-generated
  # name. We use the sequence function, and we pass it two arguments: the first is the name of the field we want to
  # generate a sequence for, and the second is an anonymous function that returns a string and interpolates a value into it.

  # the anonymous function &"Test Role #{&1}" roughly translates to: fn x -> "Test Role #{x}" end
  # So if we think about what that sequence function is doing, it’s really just doing this:
  #
  #     sequence(:name, fn x ->
  #       "Test Role #{x}"
  #     end)
  #
  # which produces something like: "Test Role 0"

  def factory(:role) do
    %Role{
      name: sequence(:name, &"Test Role #{&1}"),
      admin: false
    }
  end

  def factory(:user) do
    test_password = "test1234"
    %User{
      username: sequence(:username, &"User #{&1}"),
      email: "test@test.com",
      password: test_password,
      password_confirmation: test_password,
      password_digest: Comeonin.Bcrypt.hashpwsalt(test_password),
      role: build(:role)    # an association via the build function
    }
  end

  def factory(:post) do
    %Post{
      title: "Some Post",
      body: "And the body of some post",
      user: build(:user)
    }
  end

  def factory(:comment) do
    %Comment{
      author: "Test User",
      body: "This is a sample comment",
      approved: false,
      post: build(:post)
    }
  end

end