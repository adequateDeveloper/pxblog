# Pxblog

From following Brandon Richey's blog post at:

Part 1: https://medium.com/@diamondgfx/introduction-fe138ac6079d#.v3v4jetke

Part 2: https://medium.com/@diamondgfx/writing-a-blog-engine-in-phoenix-part-2-authorization-814c06fa7c0#.i3233t71i

Part 3: https://medium.com/@diamondgfx/writing-a-blog-engine-in-phoenix-and-elixir-part-3-adding-roles-to-our-models-3be45a4afe4b#.lpzx80ikx

Part 4: https://medium.com/@diamondgfx/writing-a-blog-engine-in-phoenix-and-elixir-part-4-adding-roles-to-our-controllers-9f4678b48468#.n33jn8ogh

Part 5: https://medium.com/@diamondgfx/mixology-exmachina-92a08dc3e954#.oiak7q26h

Part 6: https://medium.com/@diamondgfx/writing-a-blog-engine-in-phoenix-and-elixir-part-5-markdown-support-fde72badd8e1#.bhlljkaff

Part 7: https://medium.com/@diamondgfx/writing-a-blog-engine-in-phoenix-and-elixir-part-7-adding-comments-support-7dfc17dd474e#.rxcumy8ix

Part 8: https://medium.com/elixir-magic/writing-a-blog-engine-in-phoenix-and-elixir-part-8-finishing-comments-30ff95d44cea#.zh9g1ajjs

Part 9: https://medium.com/elixir-magic/writing-a-blog-engine-in-phoenix-and-elixir-part-9-live-comments-9269669a6941#.dbdpadgvv

Part 10: https://medium.com/elixir-magic/writing-a-blog-engine-in-phoenix-and-elixir-part-10-testing-channels-e6371ce4cfbe#.psd48ty5r

Debugging Tips: https://medium.com/@diamondgfx/debugging-phoenix-with-iex-pry-5417256e1d11#.jwov32xwb

Features:

 * Earmark Markdown package
 * ExMachina test data generator package
 * SemaphoreCI configuration

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: http://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

[![Build Status](https://semaphoreci.com/api/v1/adequateDeveloper/pxblog/branches/master/badge.svg)](https://semaphoreci.com/adequateDeveloper/pxblog)  
